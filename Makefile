all: latex clean

latex:
	latex Curriculo-ViniciusJarinaK.tex
	dvips -o Curriculo-ViniciusJarina.ps Curriculo-ViniciusJarinaK.dvi
	ps2pdf Curriculo-ViniciusJarina.ps Curriculo-ViniciusJarinaK.pdf
	#catdvi Curriculo-ViniciusJarina.dvi > Curriculo-ViniciusJarina.txt

clean:
	rm -f *.aux *.dvi *.nav *.snm *.toc *.vrb *.backup *.log *.out
	rm -f *.ps
	chmod -x *.tex
	chmod +x *.pdf
